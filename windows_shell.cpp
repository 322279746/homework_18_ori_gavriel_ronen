#include <Windows.h>
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Helper.h"
#include <string>
using namespace std;

void main()
{
	int length;
	LPCSTR order;
	string cd = "cd", command, partOf, created = "create", ls = "ls";
	char* path;
	string folder;
	const int _ZERO = 0;
	Helper myHelp;
	vector<string> myVec;
	WIN32_FIND_DATA ffd;

	// Get the current working directory:   
	if ((path = _getcwd(NULL, 0)) == NULL)
	{
		perror("_getcwd error");
	}

	else
	{
		cout << endl << path << ">";
	}

	getline(std::cin, command);
	myHelp.trim(command);
	myVec = myHelp.get_words(command);

	if (((cd.compare(myVec[0])) == _ZERO))
	{
		const char *cstr = myVec[1].c_str();

		if (_chdir(cstr))
		{
			switch (errno)
			{
			case ENOENT:
				printf("Unable to locate the directory: %s\n", cstr);
				break;
			case EINVAL:
				printf("Invalid buffer.\n");
				break;
			default:
				printf("Unknown error.\n");
			}
		}

		else
		{
			if ((path = _getcwd(NULL, 0)) == NULL)
			{
				perror("_getcwd error");
			}

			else
			{
				cout << endl << path << ">";
			}
		}
	}

	else if((created.compare(myVec[0]) == _ZERO))
	{
		order = myVec[1].c_str();

		HANDLE h = CreateFile(order,
			GENERIC_WRITE,
			0,             // sharing mode, none in this case
			0,             // use default security descriptor
			CREATE_ALWAYS, // overwrite if exists
			FILE_ATTRIBUTE_NORMAL,
			0);

		if (h)
		{
			CloseHandle(h);
		}

		else
		{
			std::cerr << "CreateFile() failed:" << GetLastError() << "\n";
		}
	}

	else if ((ls.compare(myVec[0]) == _ZERO))
	{
		folder = path;
		folder.append("\*");
		const char* files = folder.c_str();
		HANDLE hFind = FindFirstFile(TEXT(files), &ffd);

		if (INVALID_HANDLE_VALUE != hFind)
		{
			do
			{
				cout << ffd.cFileName << endl;
			} while (FindNextFile(hFind, &ffd) != 0);
			FindClose(hFind);
		}
		else
		{
			// Report failure.
		}

	}
	system("pause");
}